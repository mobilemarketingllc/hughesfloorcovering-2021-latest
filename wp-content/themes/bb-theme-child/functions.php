<?php

// Defines
define('FL_CHILD_THEME_DIR', get_stylesheet_directory());
define('FL_CHILD_THEME_URL', get_stylesheet_directory_uri());

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action('wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000);

add_action('wp_enqueue_scripts', function () {
    wp_enqueue_script("slick", get_stylesheet_directory_uri()."/resources/slick/slick.min.js", "", "", 1);
    wp_enqueue_script("cookie", get_stylesheet_directory_uri()."/resources/jquery.cookie.min.js", "", "", 1);
    //wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});

function replace_core_jquery_version() {
    wp_deregister_script( 'jquery' );
    // Change the URL if you want to load a local copy of jQuery from your own server.
    wp_register_script( 'jquery', "https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js", array(), '3.2.1' );
}
add_action( 'wp_enqueue_scripts', 'replace_core_jquery_version' );

//Facet Title Hook
add_filter('facetwp_shortcode_html', function ($output, $atts) {
    if (isset($atts['facet'])) {
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2);

add_filter('wp_nav_menu_items', 'do_shortcode');

// Register menus
function register_my_menus()
{
    register_nav_menus(
        array(
            'footer-1' => __('Footer Menu 1'),
            'footer-2' => __('Footer Menu 2'),
            'footer-3' => __('Footer Menu 3'),
            'footer-4' => __('Footer Menu 4'),
            'footer-5' => __('Footer Menu 5'),
            'site-map' => __('Site Map'),
        )
    );
}
add_action('init', 'register_my_menus');


 
// Enable shortcodes in text widgets
add_filter('widget_text', 'do_shortcode');


// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

function fr_img($id=0, $size="", $url=false, $attr="")
{

    //Show a theme image
    if (!is_numeric($id) && is_string($id)) {
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if (file_exists(to_path($img))) {
            if ($url) {
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if (!$id) {
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if (is_object($id)) {
        if (!empty($id->ID)) {
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if (get_post_type($id)!="attachment") {
        $id=get_post_thumbnail_id($id);
    }

    if ($id) {
        $image_url=wp_get_attachment_image_url($id, $size);
        if (!$url) {
            //If image is a SVG embed the contents so we can change the color dinamically
            if (substr($image_url, -4, 4)==".svg") {
                $image_url=str_replace(get_bloginfo("url"), ABSPATH."/", $image_url);
                $data=file_get_contents($image_url);
                echo strstr($data, "<svg ");
            } else {
                return wp_get_attachment_image($id, $size, 0, $attr);
            }
        } elseif ($url) {
            return $image_url;
        }
    }
}
 
// shortcode to show H1 google keyword fields
function new_google_keyword()
{
    if (@$_COOKIE['keyword'] ==""  && @$_COOKIE['brand'] == "") {
        return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on Flooring<h1>';
    } else {
        $keyword = $_COOKIE['keyword'];
        $brand = $_COOKIE['brand'];
        return $google_keyword = '<h1 class="googlekeyword">Save up to $500 on '.$brand.' '.$keyword.'<h1>';
    }
}
  add_shortcode('google_keyword_code', 'new_google_keyword');
  add_action('wp_head', 'cookie_gravityform_js');

  function cookie_gravityform_js()
  { // break out of php?>
  <script>
	  var brand_val ='<?php echo $_COOKIE['brand']; ?>';
	  var keyword_val = '<?php echo $_COOKIE['keyword']; ?>';  

      jQuery(document).ready(function($) {
      jQuery("#input_14_9").val(keyword_val);
      jQuery("#input_14_10").val(brand_val);
    });
  </script>
  <?php
     setcookie('keyword', '', -3600);
      setcookie('brand', '', -3600);
  }

// Action to for styling H1 tag - google keyword fields
add_action('wp_head', 'add_css_head');
function add_css_head()
{
    ?>
      <style>
          .googlekeyword {
             text-align:center;
             color: #fff;
             text-transform: capitalize;   
              /* font-size:2.5em !important; */
              font-size: 36px !important;
           }
      </style>  
   <?php
}
/**
 * Dequeue the jQuery UI styles.
 *
 * Hooked to the wp_print_styles action, with a late priority (100),
 * so that it is after the style was enqueued.
 */
function remove_pagelist_css() {
    wp_dequeue_style( 'page-list-style' );
 }
 add_action( 'wp_print_styles', 'remove_pagelist_css', 100 );

 function post_top_meta() {
    global $post;

    $settings      = FLTheme::get_settings();
   // $show_author   = 'visible' === $settings['fl-blog-post-author'] ? true : false;
    $show_date     = 'visible' === $settings['fl-blog-post-date'] ? true : false;
    $comments      = comments_open() || '0' !== get_comments_number();
    $comment_count = 'visible' === $settings['fl-blog-comment-count'] ? true : false;

    include locate_template( 'includes/post-top-meta.php' );

}

